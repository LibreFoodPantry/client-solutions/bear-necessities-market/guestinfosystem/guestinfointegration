# GuestInfoIntegration

This repo contains deployments of the GuestInfoSystem and allows users to run for testing.

## Overview

The purpose of this system is to integrate GuestInfoFrontend and GuestInfoBackend.

## Status

GuestInfoIntegration is ready to integrate all other systems in GuestInfoSystem, and is awaiting them to be ready. All projects here have been deemed stale.

1. [Icon](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration/-/issues/1#what-was-the-outcome) is a project containing an image and code to work as a desktop shortcut and application.
2. [Halted due to project readiness](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration/-/issues/3#what-was-the-outcome) is a thread containing logs of past work done on the integration system This log was last updated on May 2, 2023 by Nathan Curameng. This was followed up on by @LfpTriageBot, which deemed the project stale on February 6, 2024.
3. The GuestInfoIntegration has been [updated](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration/-/commit/f7d86ba224bf3f28f0762ac73ff702dd4dc9e504) to pull the latest builds of frontend and backend as of May 2, 2023 by Matthew Lasevoli.
4. Documentation for this project was last updated on: February 19, 2025 by Jacob Mendeszoon and Trevor Dowd.

## Setting up the Development Environment

### Windows only distribution

- Standalone deployment for Windows. Provides a console to run the guestInfoSystem on a Windows system through Docker.

### Technologies needed to set up a Development Environment

- VSCode - VSCode can be downloaded for any operating system using the link provided below.
[VSCode Setup](https://code.visualstudio.com/download)
- Docker Desktop - Docker Desktop can be installed for any operating system using the link provided below.
[Docker Desktop Setup](https://docs.docker.com/get-started/get-docker/)
- Git - Git can be downloaded for any operating system using the link provided below.
[Git Setup](https://git-scm.com/downloads)

### Installing Instructions

1. Start Docker Desktop and keep it open in the background.
2. Navigate to the GuestInfoIntegration project page [GuestInfoIntegration](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfointegration) via GitLab and click the ‘Code’ drop down button `Code -> Open in your IDE -> Visual Studio Code (HTTPS).`
3. Allow the site to open VSCode.
4. Select the location where you would like to store the project.
5. VSCode will ask ‘Would you like to open the cloned repository?’ Select ‘Open’

### Build & Run

**Make sure docker is up and running.**

**Follow these instructions to run the GuestInfoIntegration:**

1. (open new bash terminal)
2. Type the command `cd Windows`
3. Then, type the command `./run.cmd`
   - This command starts the following containers:
      - windows-backend-1
      - windows-frontend-1
      - windows-rabbitmq-1
      - windows-backend-database-1
   - If you recieve an error trying to run this command, open your Docker Desktop.
      - Go to `Settings -> Resources -> File Sharing`
      - Click the ‘+’ icon and select the project folder where you saved it.
      - Then click ‘Apply & restart’
      - Open VSCode again and retry the command

### Usage Instructions

Once running, the bash terminal will provide the user with the following options:

1. LAUNCH WEBPAGE
   - Opens Bear Necessities Market page in your default or currently open browser and jumps to it. This is a working model of the final product.
2. VIEW PROGRAM CONSOLE
   - Opens the Program Console in Powershell or Terminal emulator and jumps to it.
      - This page will reflect all information passing through the system, and is useful for troubleshooting connection errors.
3. UPDATE SYSTEM
   - Updates the currently running instance of the market webpage to reflect information changed in the database.
4. TURN OFF SYSTEM
   - Gracefully stops all containers, then says "Bye bye!". From here, pressing any key will return the user to the bash terminal.

## Folder Information

1. docs/developer
   1. Contains information about various aspects of GuestInfoIntegration that will help developers work on the project.
2. LICENSES
   1. Contains licenses so individuals using the system interact with it properly.
3. Windows
   1. Contains docker-compose.yaml and run.cmd.
   2. These are the necessary files to run and test the system.

## Tools

### Developer Guide

1. Read [LibreFoodPantry.org](https://librefoodpantry.org/) and join its Discord server.
2. [lib/openapi.yaml](https://gitlab.com/LibreFoodPantry/client-solutions/bear-necessities-market/guestinfosystem/guestinfobackend/-/blob/main/lib/openapi.yaml) contains the API specification that the server implements. This file should not be edited. If changes are needed in the API, development happens in the [source for the GuestInfoAPI](https://gitlab.com/LibreFoodPantry/client-solutions/theas-pantry/guestinfosystem/guestinfoapi).
3. Familiarize yourself with the systems used by this project (see Development Infrastructure below).
4. See the **Build & Run** and **Usage Instructions** topics in this README for common processes you'll likely follow.

### Development Infrastructure

- [Continuous Integration](docs/developer/continuous-integration.md)
- [Dependency Management](docs/developer/dependency-management.md)
- [Development Environment](docs/developer/development-environment.md)
- [Documentation System](docs/developer/documentation-system.md)
- [Version Control System](docs/developer/version-control-system.md)
- [Linting](docs/developer/linting.md)
- [Docker Desktop](https://www.docker.com/products/docker-desktop/)
- [Visual Studio Code](https://code.visualstudio.com/)

## License

By submitting this issue or commenting on this issue, or contributing any content to this issue, you certify under the Developer Certificate of Origin that the content you post may be licensed under GPLv3 (for code) or CC-BY-SA 4.0 International (for non-code content).

Last updated: November 11th, 2024
