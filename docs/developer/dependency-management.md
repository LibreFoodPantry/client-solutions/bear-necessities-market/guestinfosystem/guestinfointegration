# Dependency Management

This document outlines the dependency management practices for our project, focusing on tools and best practices to ensure consistent and reliable development environments. It covers managing dependencies for containerized components using Docker. Proper dependency management enables reproducibility, security, and easier maintenance by tracking and controlling the versions of tools and libraries used.

Most of the tools/components of this project are packaged into separate
Docker images. So we can manage the dependencies of each tool/component
separately. Managing dependencies in this project requires an
understanding of concepts related to Docker. We describe
this briefly below and provide links to help you get started.

## Docker - Managing dependencies

Docker does not come with any automated dependency management tool.
We install dependencies in Dockerfile using common Linux package managers.
These can be used to install specific package versions. Whenever possible
you should specify a specific package version (this is called pinning the
version). The version number of the base image for a Docker file should also
pinned. The article below describes the basics of pinning dependencies in
Dockerfiles.

* [Our Dockerfile Tips & Tricks](https://www.balena.io/blog/our-dockerfile-tips-tricks/)
