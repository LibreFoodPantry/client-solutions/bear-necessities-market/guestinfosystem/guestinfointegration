@echo off

:StartGuestInfoSystem
echo Starting BNM Guest Info System...
docker compose up -d
if %errorlevel% neq 0 (
	echo There was an error starting the system. Is Docker running?
	pause
	exit
)

:PrintMenu
echo [1] LAUNCH WEBPAGE
echo [2] VIEW PROGRAM CONSOLE
echo [3] UPDATE SYSTEM
echo [4] TURN OFF SYSTEM
choice /c 1234

if %errorlevel% equ 1 ( REM Launch webpage
	start http://localhost:10300
	goto PrintMenu
) else if %errorlevel% equ 2 ( REM View logs
	start cmd.exe @cmd /c docker compose logs -f
) else if %errorlevel% equ 3 ( REM Update system
	docker compose pull
) else if %errorlevel% equ 4 ( REM Turn off system
	docker compose stop
	echo Bye bye!
	pause
	exit
)

goto PrintMenu